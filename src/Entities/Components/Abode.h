struct Abode
{
	uint32_t townId;
	// If a village does not have a ABODE_STORAGE_PIT then other abodes are used by the villagers
	uint32_t foodAmount;
	uint32_t woodAmount;
};
